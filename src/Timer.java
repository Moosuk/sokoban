import javax.swing.JOptionPane;

public class Timer {
	private long tmpTime;
	private long tmpTime2;
	private long minutes = 0;
	private long nowTime;
	private String time;
	FrontEnd gui;
	
	public Timer (FrontEnd gui) {
		this.gui = gui;
	}

	/**
	 * This function count down the time and when its finished the game end
	 * @param startTime a constant
	 * @param endTime the time we need
	 */
	public void TimeDown(long startTime, long endTime) {
		String labelM = "0";
		String labelS = "0";
		long currTime = endTime-(System.currentTimeMillis()-startTime)/1000;
		nowTime = currTime;
		long tmpCurrTime = currTime;
		if(currTime == 0){
			if (gui.getGameType() == -3) {
				JOptionPane.showMessageDialog(null, "You are out of time! :(", "Please Try Again!", JOptionPane.INFORMATION_MESSAGE);
				gui.backButtonPressed();
			} else if (gui.getGameType() == -2 || gui.getGameType() == -4) {
				JOptionPane.showMessageDialog(null, "You are out of time! :( Going to next map", "Going to next map", JOptionPane.INFORMATION_MESSAGE);
				gui.skipButtonPressed();
			}
			
		}
		if(tmpCurrTime != tmpTime){
			
			minutes = (currTime-currTime % 60)/60;
			currTime = currTime-60*minutes;
			
			if(currTime >=10) {
				labelS = "";
			}
			if(minutes >= 10) {
				labelM = "";
			}
			this.time = labelM+minutes+":"+labelS+currTime;
			tmpTime = tmpCurrTime;
		}
		
	}
	
	/**
	 * check if one or the seconds we want has passed
	 * @param startTime a constant
	 * @param endTime 
	 * @return true: time passed. false: time not passed 
	 */
	public boolean isTimeChanged(long startTime, long endTime) {
		boolean changed = false;
		long currTime = endTime-(System.currentTimeMillis()-startTime)/100;
		long tmpCurrTime = currTime;
		if(tmpCurrTime != tmpTime2){
			changed = true;
			tmpTime2 = tmpCurrTime;

		}
		return changed;
	}
	
	public long getCurrTime() {
		return nowTime;
	}
	
	public String getTime(){
		return this.time;
	}
}

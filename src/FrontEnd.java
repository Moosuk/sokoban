import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class FrontEnd extends JFrame implements KeyListener {
	private static final long serialVersionUID = 1L;
	
	private GameBoard board;
	private int numRows;
	private int numCols;
	private int gameType; //-1 = none, 0 = single player, 1 = multi player
	private boolean skipState;
	
	private JPanel mainMenu;
	private JPanel gameMenu;
	private JPanel instructionsMenu;
	private JPanel creditsMenu;
	private String playerImage1;
	private String playerImage2;
	
	private final int TITLE_SIZE = 50;
	//private final int ICON_SIZE = 50;

	//private final int GAMETYPE_NONE = -1;
	private final int GAMETYPE_SINGLE = 0;
	private final int GAMETYPE_MULTI = 1;
	private final int GAMETYPE_AI = 2;
	
	//private final int GAMETYPE_SINGLE_RUN = -3;
	//private final int GAMETYPE_MULTI_RUN = -2;
	private final int GAMETYPE_AI_RUN = -4;
	private final int GAMETYPE_NONE_RUN = -5;
	
	private final String BLANK_IMAGE = "images//blank.png";
	private final String BOX_IMAGE = "images//box.png";
	private final String GOAL_IMAGE = "images//goal.png";
	private final String WALL_IMAGE = "images//wall.png";
	
	private final String PLAYER_1_DOWN_IMAGE = "images//main-front.png";
	private final String PLAYER_1_UP_IMAGE = "images//main-back.png";
	private final String PLAYER_1_LEFT_IMAGE = "images//main-left.png";
	private final String PLAYER_1_RIGHT_IMAGE = "images//main-right.png";
	
	private final String PLAYER_1_BOX_ON_GOAL_IMAGE = "images//goal-box.png";
	
	private final String PLAYER_2_DOWN_IMAGE = "images//rival-front.png";
	private final String PLAYER_2_UP_IMAGE = "images//rival-back.png";
	private final String PLAYER_2_LEFT_IMAGE = "images//rival-left.png";
	private final String PLAYER_2_RIGHT_IMAGE = "images//rival-right.png";
	
	private final String PLAYER_2_BOX_ON_GOAL_IMAGE = "images//goal-box.png";
	
	/**
	 * Constructs the UI
	 */
    public FrontEnd(int numRows, int numCols) {
    	this.numRows = numRows;
    	this.numCols = numCols;
    	
    	gameType = -1;
    	skipState = false;
    	playerImage1 = PLAYER_1_DOWN_IMAGE;
    	playerImage2 = PLAYER_2_DOWN_IMAGE;
    	
    	//we create all the components at the same time, as otherwise progressive loading does not seem to work
        initGameMenu();
        initMainMenu();
        initInstructionsMenu();
        initCreditsMenu();
        
        //sets attributes for the window regardless of menu
        setContentPane(mainMenu);
        setTitle("COMP2911");
        setLocationRelativeTo(null); //places window in the middle of the screen
        setResizable(false); //prevents user from resizing the window
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    /**
     * Creates the main menu screen
     */
    private void initMainMenu() {
    	//layout components
		mainMenu = new JPanel();
		JPanel controls = new JPanel();
		GroupLayout layout = new GroupLayout(controls);
		
		//visible components
		JLabel titleLabel = new JLabel("Pokemon Boss");
        JButton singlePlayerButton = new JButton("Single Player");
        JButton multiPlayerButton = new JButton("Multi Player");
        JButton aiButton = new JButton("Player vs AI");
        JButton instructionsButton = new JButton("Instructions");
        JButton creditsButton = new JButton("Credits");
        
        //set up buttons
        titleLabel.setFont(new Font(getName(), Font.PLAIN, TITLE_SIZE));
        singlePlayerButton.addActionListener(new ActionListener() { 
        	public void actionPerformed(ActionEvent e) { 
        		singlePlayerButtonPressed();
        	} 
        } );
        multiPlayerButton.addActionListener(new ActionListener() { 
        	public void actionPerformed(ActionEvent e) { 
        		multiPlayerButtonPressed();
      	  	} 
        } );
        aiButton.addActionListener(new ActionListener() { 
        	public void actionPerformed(ActionEvent e) { 
        		aiButtonPressed();
      	  	} 
        } );
        instructionsButton.addActionListener(new ActionListener() { 
        	public void actionPerformed(ActionEvent e) { 
        		instructionsButtonPressed();
      	  	} 
        } );
        creditsButton.addActionListener(new ActionListener() { 
        	public void actionPerformed(ActionEvent e) { 
        		creditsButtonPressed();
      	  	} 
        } );
        
        mainMenu.setLayout(new GridBagLayout());
        mainMenu.add(controls);
        
        controls.setLayout(layout);
        
        //align the components
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createSequentialGroup()
        		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
        				.addComponent(titleLabel)
        				.addComponent(singlePlayerButton)
        				.addComponent(multiPlayerButton)
        				.addComponent(aiButton)
        				.addComponent(instructionsButton)
        				.addComponent(creditsButton))
        		
        );
       
        layout.linkSize(SwingConstants.HORIZONTAL, singlePlayerButton, multiPlayerButton, aiButton, instructionsButton, creditsButton);

        layout.setVerticalGroup(layout.createSequentialGroup()
        		.addComponent(titleLabel)
        		.addGap(50)
        		.addGroup(layout.createSequentialGroup()
        				.addComponent(singlePlayerButton)
        				.addComponent(multiPlayerButton)
        				.addComponent(aiButton)
        				.addComponent(instructionsButton)
        				.addComponent(creditsButton))
        		.addGap(80)
        );

        pack();
        setSize(500,700);
	}
    
    /**
     * Creates the game menu screen
     */
    private void initGameMenu() {
    	//the full sized window
    	gameMenu = new JPanel();
    	GroupLayout windowLayout = new GroupLayout(gameMenu);
    	gameMenu.setLayout(windowLayout);
    	
    	//the components within the window
    	JPanel controls = new JPanel();
    	JLabel roundLabel = new JLabel("Round: N");
    	JLabel scoreLabel = new JLabel("P1: N | P2: N");
    	JLabel timerLabel = new JLabel("Timer: 99:99");
    	JButton resetButton = new JButton("Reset");
    	JButton backButton = new JButton("Back");
    	JButton skipButton = new JButton("Skip");
    	
    	//set up buttons
    	resetButton.addActionListener(new ActionListener() { 
    		public void actionPerformed(ActionEvent e) { 
    			resetButtonPressed();
    		} 
    	} );
    	backButton.addActionListener(new ActionListener() { 
    		public void actionPerformed(ActionEvent e) { 
    			backButtonPressed();
    		} 
    	} );
    	skipButton.addActionListener(new ActionListener() { 
    		public void actionPerformed(ActionEvent e) { 
    			skipButtonPressed();
    		} 
    	} );
    	resetButton.setFocusable(false);
    	backButton.setFocusable(false);
    	skipButton.setFocusable(false);
        
    	//align components
        windowLayout.setAutoCreateGaps(true);
        windowLayout.setAutoCreateContainerGaps(true);

        windowLayout.setHorizontalGroup(windowLayout.createSequentialGroup()
        		.addGroup(windowLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
        				.addGroup(windowLayout.createSequentialGroup()
        						.addComponent(timerLabel)
        						.addComponent(roundLabel)
        						.addComponent(scoreLabel)
        						.addComponent(resetButton)
        						.addComponent(backButton)
        						.addComponent(skipButton))
        				.addComponent(controls))
        );

        windowLayout.setVerticalGroup(windowLayout.createSequentialGroup()
        		.addGroup(windowLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        				.addComponent(timerLabel)
        				.addComponent(roundLabel)
        				.addComponent(scoreLabel)
        				.addComponent(resetButton)
        				.addComponent(backButton)
        				.addComponent(skipButton))
        		.addGap(50)
        		.addComponent(controls)
        		.addGap(80)
        );
        
        GridLayout test = new GridLayout(numRows, numCols); //creates a grid
        controls.setLayout(test);
        
        //fills the grid with blank images
        for(int i = 0; i < numRows*numCols; i++) {
        	controls.add(new JLabel(new ImageIcon(WALL_IMAGE)));
        }
        
        //adds keyboard functionality to the window
        setFocusable(true);
        removeKeyListener(this);
        addKeyListener(this);
    }
    
    /**
     * Creates the instructions menu screen
     */
    public void initInstructionsMenu() {
    	instructionsMenu = new JPanel();
    	JPanel controls = new JPanel();
		GroupLayout layout = new GroupLayout(controls);
		
		//visible components
		JLabel titleLabel = new JLabel("Instructions");
		
		JLabel spTitle = new JLabel("Single Player");
		JLabel spLine1 = new JLabel("The aim of single player is to catch all the pikachus    ");
		JLabel spLine2 = new JLabel("by moving the pokeball over the top of them.             ");
		JLabel spLine3 = new JLabel("To do this you control the player by using the WASD keys,");
		JLabel spLine4 = new JLabel("where W = up, S = down, A = left, and D = right.         ");
		
		JLabel mpTitle = new JLabel("Multi Player");
		JLabel mpLine1 = new JLabel("This mode is the same as single player,                  ");
		JLabel mpLine2 = new JLabel("except now there is a player 2.                          ");
		JLabel mpLine3 = new JLabel("This additional player moves via the arrow keys.         ");
		JLabel mpLine4 = new JLabel("The first player to win 5 rounds wins overall.           ");
		
		JLabel aiTitle = new JLabel("Player vs AI");
		JLabel aiLine1 = new JLabel("This mode is similar to multi player however player 2    ");
		JLabel aiLine2 = new JLabel("is controlled by an AI.                                  ");
		JLabel aiLine3 = new JLabel("Again the aim of this mode is to win 5 rounds before     ");
		JLabel aiLine4 = new JLabel("the other player.                                        ");
		
		String font = "Courier";
		int size = 16;
		
		//make it monospace because i cant figure out how to set it to left alligned
		spLine1.setFont(new Font(font, Font.PLAIN, size));
		spLine2.setFont(new Font(font, Font.PLAIN, size));
		spLine3.setFont(new Font(font, Font.PLAIN, size));
		spLine4.setFont(new Font(font, Font.PLAIN, size));
		mpLine1.setFont(new Font(font, Font.PLAIN, size));
		mpLine2.setFont(new Font(font, Font.PLAIN, size));
		mpLine3.setFont(new Font(font, Font.PLAIN, size));
		mpLine4.setFont(new Font(font, Font.PLAIN, size));
		aiLine1.setFont(new Font(font, Font.PLAIN, size));
		aiLine2.setFont(new Font(font, Font.PLAIN, size));
		aiLine3.setFont(new Font(font, Font.PLAIN, size));
		aiLine4.setFont(new Font(font, Font.PLAIN, size));
		
        JButton backButton = new JButton("Back");
        
        //set up button
        titleLabel.setFont(new Font(getName(), Font.PLAIN, TITLE_SIZE));
        spTitle.setFont(new Font(getName(), Font.PLAIN, 20));
        mpTitle.setFont(new Font(getName(), Font.PLAIN, 20));
        aiTitle.setFont(new Font(getName(), Font.PLAIN, 20));
        backButton.addActionListener(new ActionListener() { 
        	  public void actionPerformed(ActionEvent e) { 
        		  backButtonPressed();
        	  } 
        } );
        
        instructionsMenu.setLayout(new GridBagLayout());
        instructionsMenu.add(controls);
        
        controls.setLayout(layout);
        
        //align components
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createSequentialGroup()
        		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
        				.addComponent(titleLabel)
        				.addComponent(spTitle)
        				.addComponent(spLine1)
        				.addComponent(spLine2)
        				.addComponent(spLine3)
        				.addComponent(spLine4)
        				.addComponent(mpTitle)
        				.addComponent(mpLine1)
        				.addComponent(mpLine2)
        				.addComponent(mpLine3)
        				.addComponent(mpLine4)
        				.addComponent(aiTitle)
        				.addComponent(aiLine1)
        				.addComponent(aiLine2)
        				.addComponent(aiLine3)
        				.addComponent(aiLine4)
        				.addComponent(backButton))
        		
        );

        layout.setVerticalGroup(layout.createSequentialGroup()
        		.addComponent(titleLabel)
        		.addGap(50)
        		.addComponent(spTitle)
        		.addComponent(spLine1)
				.addComponent(spLine2)
				.addComponent(spLine3)
				.addComponent(spLine4)
				.addGap(50)
				.addComponent(mpTitle)
				.addComponent(mpLine1)
				.addComponent(mpLine2)
				.addComponent(mpLine3)
				.addComponent(mpLine4)
				.addGap(50)
				.addComponent(aiTitle)
				.addComponent(aiLine1)
				.addComponent(aiLine2)
				.addComponent(aiLine3)
				.addComponent(aiLine4)
        		.addGroup(layout.createSequentialGroup()
        				.addComponent(backButton))
        		.addGap(80)
        );

        setSize(500,700);
    }
    
    /**
     * Creates the credits menu screen
     */
    public void initCreditsMenu() {
    	creditsMenu = new JPanel();
    	JPanel controls = new JPanel();
		GroupLayout layout = new GroupLayout(controls);
		
		//visible components
		JLabel titleLabel = new JLabel("Credits");
		JLabel shawnLabel = new JLabel("Bohan Yu - Backend, Timer");
		JLabel lachlanLabel = new JLabel("Lachlan Kerr - Front End");
		JLabel phillipLabel = new JLabel("Moosuk Pyun - Backend, AI");
		JLabel tomLabel = new JLabel("Tom Castagnone - Map Generation");
        JButton backButton = new JButton("Back");
        
        //set up button
        shawnLabel.setFont(new Font(getName(), Font.PLAIN, 20));
        lachlanLabel.setFont(new Font(getName(), Font.PLAIN, 20));
        phillipLabel.setFont(new Font(getName(), Font.PLAIN, 20));
        tomLabel.setFont(new Font(getName(), Font.PLAIN, 20));
        titleLabel.setFont(new Font(getName(), Font.PLAIN, TITLE_SIZE));
        backButton.addActionListener(new ActionListener() { 
        	  public void actionPerformed(ActionEvent e) { 
        		  backButtonPressed();
        	  } 
        } );
        
        creditsMenu.setLayout(new GridBagLayout());
        creditsMenu.add(controls);
        
        controls.setLayout(layout);
        
        //align components
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createSequentialGroup()
        		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
        				.addComponent(titleLabel)
        				.addComponent(shawnLabel)
        				.addComponent(lachlanLabel)
        				.addComponent(phillipLabel)
        				.addComponent(tomLabel)
        				.addComponent(backButton))
        		
        );

        layout.setVerticalGroup(layout.createSequentialGroup()
        		.addComponent(titleLabel)
        		.addGap(50)
        		.addComponent(shawnLabel)
        		.addComponent(lachlanLabel)
        		.addComponent(phillipLabel)
        		.addComponent(tomLabel)
        		.addGap(50)
        		.addGroup(layout.createSequentialGroup()
        				.addComponent(backButton))
        		.addGap(80)
        );

        setSize(500,700);
    }
    
    /**
     * Handles the keyboard input
     */
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();

        if(key == KeyEvent.VK_A) {
            board.moveLeft();
            playerImage1 = PLAYER_1_LEFT_IMAGE;
        }
        if(key == KeyEvent.VK_D) {
            board.moveRight();
            playerImage1 = PLAYER_1_RIGHT_IMAGE;
        }
        if(key == KeyEvent.VK_W) {
        	board.moveUp();
            playerImage1 = PLAYER_1_UP_IMAGE;
        }
        if(key == KeyEvent.VK_S) {
        	board.moveDown();
            playerImage1 = PLAYER_1_DOWN_IMAGE;
        }
        
        if(key == KeyEvent.VK_LEFT && gameType == -2) {
            board.moveLeftPlayer2();
            playerImage2 = PLAYER_2_LEFT_IMAGE;
        }
        if(key == KeyEvent.VK_RIGHT && gameType == -2) {
            board.moveRightPlayer2();
            playerImage2 = PLAYER_2_RIGHT_IMAGE;
        }
        if(key == KeyEvent.VK_UP && gameType == -2) {
        	board.moveUpPlayer2();
            playerImage2 = PLAYER_2_UP_IMAGE;
        }
        if(key == KeyEvent.VK_DOWN && gameType == -2) {
        	board.moveDownPlayer2();
            playerImage2 = PLAYER_2_DOWN_IMAGE;
        }
        
        if(key == KeyEvent.VK_R) {
        	resetButtonPressed();
        }
        if(key == KeyEvent.VK_K) {
        	skipButtonPressed();
        }
        
        if(key == KeyEvent.VK_Q) {
        	pack();
        }

    	placeImagesFromBoard(board.getBoard());
        board.printBoard();
    }
    
    /**
     * Gets the single int version of a standard cartesian plane.
     * 0, 0 is the top left corner of the grid
     * @param x - horizontal distance
     * @param y - vertical distance
     * @return The single int representation
     */
    public int convertCoordToSingleInt(int row, int col) {
    	return row * numCols + col;
    }
    
    /**
     * Places an image at the specified position
     * @param icon - 0 = blank, 1 = player, 2 = box, 3 = goal, 4 = wall, 5 = box+goal
     * @param position - Position on the grid
     */
    public void placeImage(int icon, int position) {
    	//gets icon
    	String image = null;
    	switch(icon) {
    		case 0: image = BLANK_IMAGE; break;
    		case 1: image = playerImage1; break;
    		case 2: image = BOX_IMAGE; break;
    		case 3: image = GOAL_IMAGE; break;
    		case 4: image = WALL_IMAGE; break;
    		case 5: image = PLAYER_1_BOX_ON_GOAL_IMAGE; break;
    		case 6: image = playerImage1; break;
    		case 7: image = playerImage2; break;
    		case 8: image = playerImage2; break;
    		case 9: image = PLAYER_2_BOX_ON_GOAL_IMAGE; break;
    	}
    	
    	//place image
    	JPanel pane = (JPanel) gameMenu.getComponent(6); 
    	JLabel newCell = (JLabel) pane.getComponent(position);
    	
    	newCell.setIcon(new ImageIcon(image));
    }
    
    /**
     * Places all the images on the board
     * @param board - The board where the image values are stored
     */
    public void placeImagesFromBoard(int[][] board) {
    	//col = y-axis
    	//row = x-axis
    	for(int row = 0; row < numRows; row++) {
    		for (int col = 0; col < numCols; col++) {
    			placeImage(board[row][col], convertCoordToSingleInt(row,col));
    		}
    	}
    }
    
    /**
     * Creates a randomised board aray
     * @return Randomised board
     */
    public int[][] randomisedBoard() {
    	int[][] newBoard = new int[numRows][numCols];
    	for(int row = 0; row < numRows; row++) {
    		for(int col = 0; col < numCols; col++) {
    			newBoard[row][col] = ThreadLocalRandom.current().nextInt(0, 9 + 1);
    		}
    	}
    	return newBoard;
    }
    
    /**
     * Sets the timer
     * @param text - The timer text to display
     */
    public void setTimer(String text) {
    	JLabel timer = (JLabel) gameMenu.getComponent(0);
    	timer.setText("Timer: " + text);
    }
    
    /**
     * Changes the number of rows on the board, then re initialises the game menu
     * @param numRows - The new number of rows
     */
    public void setNumRows(int numRows) {
    	this.numRows = numRows;
    	initGameMenu();
    	
    	setVisible(false);
		setContentPane(gameMenu);
		pack();
		setVisible(true);
    }
    
    /**
     * Changes the number of columns on the board, then re initialises the game menu
     * @param numCols - The new number of columns
     */
    public void setNumCols(int numCols) {
    	this.numCols = numCols;
    	initGameMenu();
    	
    	setVisible(false);
		setContentPane(gameMenu);
		pack();
		setVisible(true);
    }
    
    /**
     * Sets the board for the gui to check and pull info from
     * @param board - The board to set
     */
    public void setBoard(GameBoard board) {
    	this.board = board;
    }
    
    /**
     * Gets the number of columns for the current grid
     * @return - Number of columns
     */
    public int getNumCols() {
    	return numCols;
    }
    
    /**
     * Gets the number of rows for the current grid
     * @return - Number of rows
     */
    public int getNumRows() {
    	return numRows;
    }
    
    /**
	 * Need this because we implement the keylistener class
     */
	public void keyReleased(KeyEvent e) {
		//not used at the moment
	}
	
	/**
	 * Need this because we implement the keylistener class
	 */
	public void keyTyped(KeyEvent e) {
		//not used at the moment
	}
	
	/**
	 * Changes the content panel to the game menu and then activates the single player gametype
	 */
	public void singlePlayerButtonPressed() {		
		setVisible(false);
		setContentPane(gameMenu);
		pack();
		setVisible(true);
		
		gameType = GAMETYPE_SINGLE;
	}
	
	/**
	 * Changes the content panel to the game menu and then activates the multi player gametype
	 */
	public void multiPlayerButtonPressed() {
		setVisible(false);
		setContentPane(gameMenu);
		pack();
		setVisible(true);
		
		gameType = GAMETYPE_MULTI;
	}
	
	/**
	 * Changes the content panel to the game menu and then activates the ai gametype
	 */
	public void aiButtonPressed() {
		setVisible(false);
		setContentPane(gameMenu);
		pack();
		setVisible(true);
		
		gameType = GAMETYPE_AI;
	}
	
	/**
	 * Changes the content panel to the instructions menu
	 */
	public void instructionsButtonPressed() {
		setVisible(false);
		setContentPane(instructionsMenu);
		pack();
		setVisible(true);
	}
	
	/**
	 * Changes the content panel to the credits menu
	 */
	public void creditsButtonPressed() {
		setVisible(false);
		setContentPane(creditsMenu);
		setSize(500, 700);
		setVisible(true);
	}
	
	/**
	 * Changes the content panel to the main menu and then sets the gametype to none_run
	 */
	public void backButtonPressed() {
		setVisible(false);
		setContentPane(mainMenu);
		setSize(500, 700);
		setVisible(true);
		board.resetBoard();
		
		gameType = GAMETYPE_NONE_RUN;
	}
	
	/**
	 * Sets the skip state to true
	 */
	public void skipButtonPressed() {
		skipState = true;
	}
	
	/**
	 * Resets the current board to its original state
	 */
	public void resetButtonPressed() {
		if (gameType != GAMETYPE_AI_RUN) {
			board.resetBoard();
			placeImagesFromBoard(board.getBoard());
		}
	}
	
	/**
	 * Gets the current gameType. 
	 * -1 = none, 0 = single player, 1 = multiplayer
	 * @return The current gameType
	 */
	public int getGameType() {
		return gameType;
	}
	
	/**
	 * Sets a new gametype
	 * @param gameType - The gametype to set
	 */
	public void setGameType(int gameType) {
		this.gameType = gameType;
	}
	
	/**
	 * Returns whether the user has pressed the skip button
	 * @return - The skip state
	 */
	public boolean getSkipState() {
		return skipState;
	}
	
	/**
	 * Sets the skip state, used for returning it to false
	 * @param skipState - The new skip state
	 */
	public void setSkipState(boolean skipState) {
		this.skipState = skipState;
	}
	
	/**
	 * Sets the image for player 2, we need this as the ai does not actually press any buttons
	 * @param direction - The direction to display
	 */
	public void setPlayerImage2(String direction) {
		switch (direction) {
			case "left": 	playerImage2 = PLAYER_2_LEFT_IMAGE; break;
			case "right": 	playerImage2 = PLAYER_2_RIGHT_IMAGE; break;
			case "down": 	playerImage2 = PLAYER_2_DOWN_IMAGE; break;
			case "up": 		playerImage2 = PLAYER_2_UP_IMAGE; break;
		}
	}
	
	/**
	 * Sets the round to display on the screen
	 * @param round - Number to display
	 */
	public void setRound(int round) {
		JLabel timer = (JLabel) gameMenu.getComponent(1);
    	timer.setText("Round: " + round);
	}
	
	/**
	 * Sets the score to display on the screen, use -1 for no display
	 * @param player1 - The score for player 1
	 * @param player2 - The score for player 2
	 */
	public void setScore(int player1, int player2) {
		JLabel timer = (JLabel) gameMenu.getComponent(2);
		if (player1 != -1)
			timer.setText("P1: " + player1 + " | P2: " + player2);
		else
			timer.setText("");
	}
}
import java.util.ArrayList;
import java.util.Iterator;

/**
 * GameBoard class for saving board information and methods
 * @author 
 *
 */
public class GameBoard {
	private static int emptySpace = 0;
	private static int person = 1;
	private static int box = 2;
	private static int goal = 3;
	//private static int wall = 4; never used
	private static int goalBox = 5;
	private static int goalPerson = 6;
	private static int person2 = 7;
	private static int goalPerson2 = 8;
	private static int goalBox2 = 9;
	public int [][] originBoard;
	public Position originStart;
	public Position originPlayer2Start;
	public int [][] board;
	private int row;
	private int column;
	private String result;
	int numGoal = 0;
	boolean multi = false;
	Position character;
	Position character2;
	
	public GameBoard(int rowSize, int columnSize) {
		this.board = new int[rowSize][columnSize];
		for (int i = 0; i < rowSize; i++) {
			for (int j = 0; j < columnSize; j++) {
				this.board[i][j] = 0;
			}
		}
		this.row = rowSize;
		this.column = columnSize;
		this.character = new Position(0, 0);
		this.character2 = new Position(0, 0);
		
		
	}
	
	/**
	 * Setter for setting multi-play mode
	 */
	public void setMultiOn() {
		this.multi = true;
	}
	
	/**
	 * Getter for getting positions of features (goal, character, box etc)
	 * @return ArrayList<Position> featurePos
	 */
	public ArrayList<Position> getFeaturePosition(){
		ArrayList<Position> featurePos = new ArrayList<Position>();
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				if (this.board[i][j] == box) {
					Position b = new Position(i, j);
					b.setType("box");
					featurePos.add(b);
				} else if (this.board[i][j] == goal) {
					Position g = new Position(i, j);
					g.setType("goal");
					featurePos.add(g);
				} else if (this.board[i][j] == goalBox) {
					Position gb = new Position(i, j);
					gb.setType("goalBox");
					featurePos.add(gb);
				} else if (this.board[i][j] == goalPerson) {
					Position gp = new Position(i, j);
					gp.setType("goalPerson");
					featurePos.add(gp);
				}
			}
		}
		return featurePos;
		
	}
	
	/**
	 * Setter for putting features into Gameboard
	 * @param featurePos
	 */
	public void setFeature(ArrayList<Position> featurePos) {
		Iterator<Position> itr = featurePos.iterator();
		while (itr.hasNext()) {
			Position feature = itr.next();
			if (feature.getType() == "character") {
				this.setStartingLocation(feature.getRow(), feature.getColumn());
			} else if (feature.getType() == "box") {
				this.setBox(feature.getRow(), feature.getColumn());
			} else if (feature.getType() == "goal") {
				this.setGoal(feature.getRow(), feature.getColumn());
			} else if (feature.getType() == "goalBox") {
				this.board[feature.getRow()][feature.getColumn()] = goalBox;
			} else if (feature.getType() == "goalPerson") {
				this.board[feature.getRow()][feature.getColumn()] = goalPerson;
			}
		}
	}
	
	/**
	 * Returns possible movements in String array
	 * Used for AI
	 * @return ArrayList<String> moves
	 */
	// Return possible movements
	public ArrayList<String> getMovement(){
		ArrayList<String> moves = new ArrayList<String>();
		int charRow = character.getRow();
		int charCol = character.getColumn();
		
		// Check if left movement is possible
		if (character.getColumn() > 0) {
			if (board[charRow][charCol - 1] == box || board[charRow][charCol - 1] == goalBox) {
				if (charCol > 1) {
					if (board[charRow][charCol - 2] == emptySpace || board[charRow][charCol - 2] == goal) {
						moves.add("left");
					}
				}
			} else if (board[charRow][charCol - 1] == emptySpace || board[charRow][charCol - 1] == goal) {
				moves.add("left");
			}
		}
		// Check if right movement is possible
		if (character.getColumn() < this.column - 1) {
			if (board[charRow][charCol + 1] == emptySpace || board[charRow][charCol + 1] == goal) {
				moves.add("right");
				
			} else if (board[charRow][charCol + 1] == box || board[charRow][charCol + 1] == goalBox) {
				if (charCol < this.column - 2) {
					if (board[charRow][charCol + 2] == emptySpace || board[charRow][charCol + 2] == goal) {
						moves.add("right");
					}
				}
			}
		}
		// Check if up movement is possible
		if (character.getRow() > 0) {
			if (board[charRow - 1][charCol] == box || board[charRow - 1][charCol] == goalBox) {
				if (charRow > 1) {
					if (board[charRow - 2][charCol] == emptySpace || board[charRow - 2][charCol] == goal) {
						moves.add("up");
					}
				}
			} else if (board[charRow - 1][charCol] == emptySpace || board[charRow - 1][charCol] == goal) {
				moves.add("up");
			}
		}
		
		// Check if down movement is possible
		if (character.getRow() < this.row - 1) {
			if (board[charRow + 1][charCol] == box || board[charRow + 1][charCol] == goalBox) {
				if (charRow < this.row-2) {
					if (board[charRow + 2][charCol] == emptySpace || board[charRow + 2][charCol] == goal) {
						moves.add("down");
					}

				}
			} else if (board[charRow + 1][charCol] == emptySpace|| board[charRow + 1][charCol] == goal) {
				moves.add("down");
			}
		}
		
		return moves;
	}
	
	/**
	 * Method for player1 left movement
	 */
	public void moveLeft(){
		if (character.getColumn() > 0) {
			int charRow = character.getRow();
			int charCol = character.getColumn();
			
			// Check if box on the left of character
			if (board[charRow][charCol-1] == box) {
				if (charCol > 1) {
					if (board[charRow][charCol - 2] == emptySpace){
						board[charRow][charCol - 1] = person;
						board[charRow][charCol - 2] = box;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character.setColumn(character.getColumn() - 1);
					}// if goal on the left of box
					else if (board[charRow][charCol - 2] == goal){
						board[charRow][charCol - 1] = person;
						board[charRow][charCol - 2] = goalBox;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}						character.setColumn(character.getColumn() - 1);
					}
				}
			}// Move if goalBox on the left
			else if(board[charRow][charCol-1] == goalBox){
				if (charCol > 1) {
					if (board[charRow][charCol - 2] == emptySpace){
						board[charRow][charCol - 1] = goalPerson;
						board[charRow][charCol - 2] = box;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character.setColumn(character.getColumn() - 1);
					}
					else if (board[charRow][charCol - 2] == goal){
						board[charRow][charCol - 1] = goalPerson;
						board[charRow][charCol - 2] = goalBox;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}						character.setColumn(character.getColumn() - 1);
					}
				}
			}
			// Move if empty space left
			else if (board[charRow][charCol - 1] == emptySpace) {
				if(board[charRow][charCol] == goalPerson){
					board[charRow][charCol - 1] = person;
					board[charRow][charCol] = goal;
					character.setColumn(character.getColumn() - 1);
				}
				else{
				board[charRow][charCol - 1] = person;
				board[charRow][charCol] = emptySpace;
				character.setColumn(character.getColumn() - 1);
				}
			}// Move if goal left 
			else if (board[charRow][charCol - 1] == goal) {
				board[charRow][charCol - 1] = goalPerson;
				if(board[charRow][charCol] == goalPerson){
					board[charRow][charCol] = goal;
				} else {
					board[charRow][charCol] = emptySpace;
				}
				character.setColumn(character.getColumn() - 1);
			}
		}
	}
	
	/**
	 * Method for player 2 left movement
	 */
	public void moveLeftPlayer2(){
		if (character2.getColumn() > 0) {
			int charRow = character2.getRow();
			int charCol = character2.getColumn();
			
			// Check if box on the left of character
			if (board[charRow][charCol-1] == box) {
				if (charCol > 1) {
					if (board[charRow][charCol - 2] == emptySpace){
						board[charRow][charCol - 1] = person2;
						board[charRow][charCol - 2] = box;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character2.setColumn(character2.getColumn() - 1);
					}// if goal on the left of box
					else if (board[charRow][charCol - 2] == goal){
						board[charRow][charCol - 1] = person2;
						board[charRow][charCol - 2] = goalBox2;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}						
						character2.setColumn(character2.getColumn() - 1);
					}
				}
			}// Move if goalBox on the left
			else if(board[charRow][charCol-1] == goalBox2){
				if (charCol > 1) {
					if (board[charRow][charCol - 2] == emptySpace){
						board[charRow][charCol - 1] = goalPerson2;
						board[charRow][charCol - 2] = box;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character2.setColumn(character2.getColumn() - 1);
					}
					else if (board[charRow][charCol - 2] == goal){
						board[charRow][charCol - 1] = goalPerson2;
						board[charRow][charCol - 2] = goalBox2;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}						
						character2.setColumn(character2.getColumn() - 1);
					}
				}
			}
			// Move if empty space left
			else if (board[charRow][charCol - 1] == emptySpace) {
				if(board[charRow][charCol] == goalPerson2){
					board[charRow][charCol - 1] = person2;
					board[charRow][charCol] = goal;
					character2.setColumn(character2.getColumn() - 1);
				}
				else{
				board[charRow][charCol - 1] = person2;
				board[charRow][charCol] = emptySpace;
				character2.setColumn(character2.getColumn() - 1);
				}
			}// Move if goal left 
			else if (board[charRow][charCol - 1] == goal) {
				board[charRow][charCol - 1] = goalPerson2;
				if(board[charRow][charCol] == goalPerson2){
					board[charRow][charCol] = goal;
				} else {
					board[charRow][charCol] = emptySpace;
				}
				character2.setColumn(character2.getColumn() - 1);
			}
		}
	}
	
	/**
	 * Method for player 1 right movement
	 */
	public void moveRight(){
		if (character.getColumn() < this.column - 1) {
			int charRow = character.getRow();
			int charCol = character.getColumn();
			
			// Move if empty space right
			if (board[charRow][charCol + 1] == emptySpace){
				if(board[charRow][charCol] == goalPerson){
					board[charRow][charCol + 1] = person;
					board[charRow][charCol] = goal;
					character.setColumn(character.getColumn() + 1);
				}else {
					board[charRow][charCol + 1] = person;
					board[charRow][charCol] = emptySpace;
					character.setColumn(character.getColumn() + 1);
				}
				// Move if box on right
			} else if (board[charRow][charCol + 1] == box) {
				// Check if there is space right right to prevent null pointer error
				if (charCol < this.column - 2) {
					if (board[charRow][charCol + 2] == emptySpace){
						board[charRow][charCol + 1] = person;
						board[charRow][charCol + 2] = box;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character.setColumn(character.getColumn() + 1);
					}
					else if(board[charRow][charCol + 2] == goal) {
						board[charRow][charCol + 1] = person;
						board[charRow][charCol + 2] = goalBox;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character.setColumn(character.getColumn() + 1);
					}
				}
			} // move if goalBox on the right
			 else if (board[charRow][charCol + 1] == goalBox) {
					// Check if there is space right right to prevent null pointer error
					if (charCol < this.column - 2) {
						if (board[charRow][charCol + 2] == emptySpace){
							board[charRow][charCol + 1] = goalPerson;
							board[charRow][charCol + 2] = box;
							if (board[charRow][charCol] == goalPerson){
								board[charRow][charCol] = goal;
							}else{
								board[charRow][charCol] = emptySpace;
							}
							character.setColumn(character.getColumn() + 1);
						}
						else if(board[charRow][charCol + 2] == goal) {
							board[charRow][charCol + 1] = goalPerson;
							board[charRow][charCol + 2] = goalBox;
							if (board[charRow][charCol] == goalPerson){
								board[charRow][charCol] = goal;
							}else{
								board[charRow][charCol] = emptySpace;
							}
							character.setColumn(character.getColumn() + 1);
						}
					}
				} 	
			// Move if goal on right
			else if (board[charRow][charCol + 1] == goal) {
				board[charRow][charCol + 1] = goalPerson;
				if(board[charRow][charCol] == goalPerson){
					board[charRow][charCol] = goal;
				} else {
					board[charRow][charCol] = emptySpace;
				}
				character.setColumn(character.getColumn() + 1);
			}
		}
	}
	
	/**
	 * Method for player 2 right movement
	 */
	public void moveRightPlayer2(){
		if (character2.getColumn() < this.column - 1) {
			int charRow = character2.getRow();
			int charCol = character2.getColumn();
			
			// Move if empty space right
			if (board[charRow][charCol + 1] == emptySpace){
				if(board[charRow][charCol] == goalPerson2){
					board[charRow][charCol + 1] = person2;
					board[charRow][charCol] = goal;
					character2.setColumn(character2.getColumn() + 1);
				}else {
					board[charRow][charCol + 1] = person2;
					board[charRow][charCol] = emptySpace;
					character2.setColumn(character2.getColumn() + 1);
				}
				// Move if box on right
			} else if (board[charRow][charCol + 1] == box) {
				// Check if there is space right right to prevent null pointer error
				if (charCol < this.column - 2) {
					if (board[charRow][charCol + 2] == emptySpace){
						board[charRow][charCol + 1] = person2;
						board[charRow][charCol + 2] = box;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character2.setColumn(character2.getColumn() + 1);
					}
					else if(board[charRow][charCol + 2] == goal) {
						board[charRow][charCol + 1] = person2;
						board[charRow][charCol + 2] = goalBox2;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character2.setColumn(character2.getColumn() + 1);
					}
				}
			} // move if goalBox on the right
			 else if (board[charRow][charCol + 1] == goalBox2) {
					// Check if there is space right right to prevent null pointer error
					if (charCol < this.column - 2) {
						if (board[charRow][charCol + 2] == emptySpace){
							board[charRow][charCol + 1] = goalPerson2;
							board[charRow][charCol + 2] = box;
							if (board[charRow][charCol] == goalPerson2){
								board[charRow][charCol] = goal;
							}else{
								board[charRow][charCol] = emptySpace;
							}
							character2.setColumn(character2.getColumn() + 1);
						}
						else if(board[charRow][charCol + 2] == goal) {
							board[charRow][charCol + 1] = goalPerson2;
							board[charRow][charCol + 2] = goalBox2;
							if (board[charRow][charCol] == goalPerson2){
								board[charRow][charCol] = goal;
							}else{
								board[charRow][charCol] = emptySpace;
							}
							character2.setColumn(character2.getColumn() + 1);
						}
					}
				} 	
			// Move if goal on right
			else if (board[charRow][charCol + 1] == goal) {
				board[charRow][charCol + 1] = goalPerson2;
				if(board[charRow][charCol] == goalPerson2){
					board[charRow][charCol] = goal;
				} else {
					board[charRow][charCol] = emptySpace;
				}
				character2.setColumn(character2.getColumn() + 1);
			}
		}
	}
	
	/**
	 * Method for player 1 up movement
	 */
	public void moveUp(){
		if (character.getRow() > 0) {
			int charRow = character.getRow();
			int charCol = character.getColumn();
			
			// Check if box on the top of character
			if (board[charRow - 1][charCol] == box) {
				if (charRow > 1) {
					if (board[charRow - 2][charCol] == emptySpace) {
						board[charRow - 1][charCol] = person;
						board[charRow - 2][charCol] = box;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character.setRow(character.getRow() - 1);
					}
					else if(board[charRow - 2][charCol] == goal) {
						board[charRow - 1][charCol] = person;
						board[charRow - 2][charCol] = goalBox;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character.setRow(character.getRow() - 1);
					}
				} 
			}// check if up is a goalbox
			else if (board[charRow - 1][charCol] == goalBox) {
				if (charRow > 1) {
					if (board[charRow - 2][charCol] == emptySpace) {
						board[charRow - 1][charCol] = goalPerson;
						board[charRow - 2][charCol] = box;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character.setRow(character.getRow() - 1);
					}
					else if(board[charRow - 2][charCol] == goal) {
						board[charRow - 1][charCol] = goalPerson;
						board[charRow - 2][charCol] = goalBox;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character.setRow(character.getRow() - 1);
					}
				} 
			}// Move if emptySpace top
			else if (board[charRow-1][charCol] == emptySpace) {
				if(board[charRow][charCol] == goalPerson){
					board[charRow-1][charCol] = person;
					board[charRow][charCol] = goal;
					character.setRow(character.getRow() - 1);
				} else {
					board[charRow-1][charCol] = person;
					board[charRow][charCol] = emptySpace;
					character.setRow(character.getRow() - 1);
				}
			} // Move if goal top
			else if (board[charRow-1][charCol] == goal) {
				board[charRow-1][charCol] = goalPerson;
				if(board[charRow][charCol] == goalPerson){
					board[charRow][charCol] = goal;
				} else {
					board[charRow][charCol] = emptySpace;
				}
				character.setRow(character.getRow() - 1);
			}
		}
	}
	
	/**
	 * Method for player 2 up movement
	 */
	public void moveUpPlayer2(){
		if (character2.getRow() > 0) {
			int charRow = character2.getRow();
			int charCol = character2.getColumn();
			
			// Check if box on the top of character
			if (board[charRow - 1][charCol] == box) {
				if (charRow > 1) {
					if (board[charRow - 2][charCol] == emptySpace) {
						board[charRow - 1][charCol] = person2;
						board[charRow - 2][charCol] = box;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character2.setRow(character2.getRow() - 1);
					}
					else if(board[charRow - 2][charCol] == goal) {
						board[charRow - 1][charCol] = person2;
						board[charRow - 2][charCol] = goalBox2;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character2.setRow(character2.getRow() - 1);
					}
				} 
			}// check if up is a goalbox
			else if (board[charRow - 1][charCol] == goalBox2) {
				if (charRow > 1) {
					if (board[charRow - 2][charCol] == emptySpace) {
						board[charRow - 1][charCol] = goalPerson2;
						board[charRow - 2][charCol] = box;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character2.setRow(character2.getRow() - 1);
					}
					else if(board[charRow - 2][charCol] == goal) {
						board[charRow - 1][charCol] = goalPerson2;
						board[charRow - 2][charCol] = goalBox2;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character2.setRow(character2.getRow() - 1);
					}
				} 
			}// Move if emptySpace top
			else if (board[charRow-1][charCol] == emptySpace) {
				if(board[charRow][charCol] == goalPerson2){
					board[charRow-1][charCol] = person2;
					board[charRow][charCol] = goal;
					character2.setRow(character2.getRow() - 1);
				} else {
					board[charRow-1][charCol] = person2;
					board[charRow][charCol] = emptySpace;
					character2.setRow(character2.getRow() - 1);
				}
			} // Move if goal top
			else if (board[charRow-1][charCol] == goal) {
				board[charRow-1][charCol] = goalPerson2;
				if(board[charRow][charCol] == goalPerson2){
					board[charRow][charCol] = goal;
				} else {
					board[charRow][charCol] = emptySpace;
				}
				character2.setRow(character2.getRow() - 1);
			}
		}
	}

	/**
	 * Method for player 1 down movement
	 */
	public void moveDown(){
		if (character.getRow() < this.row - 1) {
			int charRow = character.getRow();
			int charCol = character.getColumn();
			// Check if box is under the character
			if (board[charRow+1][charCol] == box) {
				if (charRow < this.row-2) {
					if (board[charRow+2][charCol] == emptySpace) {
						board[charRow+1][charCol] = person;
						board[charRow+2][charCol] = box;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character.setRow(character.getRow() + 1);

					}
					else if (board[charRow+2][charCol] == goal) {
						board[charRow+1][charCol] = person;
						board[charRow+2][charCol] = goalBox;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character.setRow(character.getRow() + 1);

					}
				} 
			}// move if down is goal box
			else if (board[charRow+1][charCol] == goalBox) {
				if (charRow < this.row-2) {
					if (board[charRow+2][charCol] == emptySpace) {
						board[charRow+1][charCol] = goalPerson;
						board[charRow+2][charCol] = box;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character.setRow(character.getRow() + 1);

					}
					else if (board[charRow+2][charCol] == goal) {
						board[charRow+1][charCol] = goalPerson;
						board[charRow+2][charCol] = goalBox;
						if (board[charRow][charCol] == goalPerson){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character.setRow(character.getRow() + 1);

					}
				} 
			}
			// Move if empty under
			else if (board[charRow+1][charCol] == emptySpace) {
				if(board[charRow][charCol] == goalPerson) {
					board[charRow+1][charCol] = person;
					board[charRow][charCol] = goal;
					character.setRow(character.getRow() + 1);
				}else {
					board[charRow+1][charCol] = person;
					board[charRow][charCol] = emptySpace;
					character.setRow(character.getRow() + 1);
				}
				
			}// move if goal under
			else if (board[charRow+1][charCol] == goal) {
				board[charRow+1][charCol] = goalPerson;
				if(board[charRow][charCol] == goalPerson){
					board[charRow][charCol] = goal;
				} else {
					board[charRow][charCol] = emptySpace;
				}
				character.setRow(character.getRow() + 1);
				
			}
		}
	}
	
	/**
	 * Method for player 2 down movement
	 */
	public void moveDownPlayer2(){
		if (character2.getRow() < this.row - 1) {
			int charRow = character2.getRow();
			int charCol = character2.getColumn();
			// Check if box is under the character
			if (board[charRow+1][charCol] == box) {
				if (charRow < this.row-2) {
					if (board[charRow+2][charCol] == emptySpace) {
						board[charRow+1][charCol] = person2;
						board[charRow+2][charCol] = box;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character2.setRow(character2.getRow() + 1);

					}
					else if (board[charRow+2][charCol] == goal) {
						board[charRow+1][charCol] = person2;
						board[charRow+2][charCol] = goalBox2;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character2.setRow(character2.getRow() + 1);

					}
				} 
			}// move if down is goal box
			else if (board[charRow+1][charCol] == goalBox2) {
				if (charRow < this.row-2) {
					if (board[charRow+2][charCol] == emptySpace) {
						board[charRow+1][charCol] = goalPerson2;
						board[charRow+2][charCol] = box;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character2.setRow(character2.getRow() + 1);

					}
					else if (board[charRow+2][charCol] == goal) {
						board[charRow+1][charCol] = goalPerson2;
						board[charRow+2][charCol] = goalBox2;
						if (board[charRow][charCol] == goalPerson2){
							board[charRow][charCol] = goal;
						}else{
							board[charRow][charCol] = emptySpace;
						}
						character2.setRow(character2.getRow() + 1);

					}
				} 
			}
			// Move if empty under
			else if (board[charRow+1][charCol] == emptySpace) {
				if(board[charRow][charCol] == goalPerson2) {
					board[charRow+1][charCol] = person2;
					board[charRow][charCol] = goal;
					character2.setRow(character2.getRow() + 1);
				}else {
					board[charRow+1][charCol] = person2;
					board[charRow][charCol] = emptySpace;
					character2.setRow(character2.getRow() + 1);
				}
				
			}// move if goal under
			else if (board[charRow+1][charCol] == goal) {
				board[charRow+1][charCol] = goalPerson2;
				if(board[charRow][charCol] == goalPerson2){
					board[charRow][charCol] = goal;
				} else {
					board[charRow][charCol] = emptySpace;
				}
				character2.setRow(character2.getRow() + 1);
				
			}
		}
	}

	/**
	 * Getter for getting row size of the board
	 * @return size of row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Setter for setting row size of the board
	 * @param row
	 */
	public void setRow(int row) {
		this.row = row;
	}

	/**
	 * Getter for getting column size of the board
	 * @return
	 */
	public int getColumn() {
		return column;
	}
	
	/**
	 * Setter for setting column size of the board
	 * @param column
	 */
	public void setColumn(int column) {
		this.column = column;
	}
	
	/**
	 * Getter for getting Position of character
	 * @return Position character
	 */
	public Position getCharacter() {
		return character;
	}
	
	/**
	 * Getter for clone of character position
	 * @return Position pos
	 */
	public Position getCloneChar() {
		Position pos = new Position(character.getRow(), character.getColumn());
		pos.setType("character");
		return pos;
	}
	
	/**
	 * Getter for clone of player 2 character position
	 * @return Position pos
	 */
	public Position getPlayer2CloneChar() {
		Position pos = new Position(character2.getRow(), character2.getColumn());
		pos.setType("character");
		return pos;
	}

	/**
	 * Setter for setting position of character
	 * @param character
	 */
	public void setCharacter(Position character) {
		this.character = character;
	}

	/**
	 * Stores number of goals in the map
	 */
	public void storeNumberOfGoals() {
		
		for (int i = 0; i < row; i++) {
			for(int j = 0; j < column; j++) {
				if (this.board[i][j] == 3 || this.board[i][j] == 5 || this.board[i][j] == 6 || this.board[i][j] == 8 || this.board[i][j] == 9) {
					numGoal++;
				}
			}
		}
	}
	
	/**
	 * Checks if the game is finished
	 * @return true if finished
	 * 		   false if not finished
	 */
	public boolean checkFinish(){
		boolean finish = true;
		//int numGb = 0;
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				if (this.board[i][j] == goal || this.board[i][j] == goalPerson) {
					finish = false;
				}
			}
		}
		//System.out.println("++"+numGb+"++");
		/**if(numGb == numGoal) {
			finish = true;
		}**/
		return finish;
	}
	
	/**
	 * Checks if game is finished in multiplayer mode
	 * @return true if game is finished
	 * 		   false if game is not finished
	 */
	public boolean checkFinishMulti() {
		/*if (this.multi == false) {
			return false;
		}*/
		boolean finish = false;
		//System.out.println("_____"+numGoal);
		int numGb = 0;
		int numGb2 = 0;
		for (int i = 0; i < row; i++) {
			for(int j = 0; j < column; j++) {
				if (this.board[i][j] == goalBox ) {
					numGb++;
				}
				if (this.board[i][j] == goalBox2) {
					numGb2++;
				}
			}
		}
		//System.out.println("++"+numfdwdGb+"++"+numGb2);
		if(numGb == numGoal/2 && multi == true) {
			this.result = "Player1";
			finish = true;
		} else if (numGb2 == numGoal/2) {
			this.result = "Player2";
			finish = true;
		}
		return finish;
	}
	
	/**
	 * Returns the result of who won the game
	 * @return player 1 or player 2 in String
	 */
	public String getResult() {
		return this.result;
	}
	
	/**
	 * Resets the board in to original state
	 */
	public void resetBoard() {
		this.copyBoard(this.originBoard);
		this.setStartingLocation(this.originStart.getRow(), this.originStart.getColumn());
		if (this.originPlayer2Start != null)
			this.setPlayer2StartingLocation(this.originPlayer2Start.getRow(), this.originPlayer2Start.getColumn());
		this.printBoard();
	}
	
	/**
	 * Set starting location of player 1
	 * @param rowX
	 * @param columnY
	 */
	public void setStartingLocation (int rowX, int columnY) {
		this.board[rowX][columnY] = 1;
		this.character.setColumn(columnY);
		this.character.setRow(rowX);
		this.character.setType("character");
		this.originStart = this.getCloneChar();

		
	}
	
	/**
	 * Set the starting location of player 2
	 * @param rowX
	 * @param columnY
	 */
	public void setPlayer2StartingLocation (int rowX, int columnY) {
		
		this.board[rowX][columnY] = 7;
		this.character2.setColumn(columnY);
		this.character2.setRow(rowX);
		this.character2.setType("character2");
		this.originPlayer2Start = this.getPlayer2CloneChar();
	}
	
	/**
	 * Setter for setting boxes
	 * @param rowX
	 * @param columnY
	 */
	public void setBox(int rowX, int columnY) {
		this.board[rowX][columnY] = 2;
	}
	
	/**
	 * Setter for setting goals
	 * @param rowX
	 * @param columnY
	 */
	public void setGoal(int rowX, int columnY) {
		this.board[rowX][columnY] = 3;
	}
	
	/**
	 * Setter for setting walls
	 * @param rowX
	 * @param columnY
	 */
	public void setWall(int rowX, int columnY) {
		this.board[rowX][columnY] = 4;
	}
	
	/**
	 * Clears features of board, hence does not clear wall or empty space
	 */
	public void clearBoard() {
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				if (this.board[i][j] != 0 && this.board[i][j] != 4) {
					this.board[i][j] = 0;
				}
			}
		}
	}
	
	/**
	 * Prints board for debugging
	 */
	public void printBoard() {
		for (int i = 0; i < row; i++) {
			System.out.println("");
			for (int j = 0; j < column; j++) {
				System.out.print(Integer.toString(this.board[i][j]) + " ");
			}
		}
		System.out.println("");
	}
	
	/**
	 * Getter for getting original board
	 * @return int[][] newBoard
	 */
	public int[][] getOriginalBoard() {
		int[][] newBoard = new int[row][column];
		for (int i = 0; i < row; i++) {
			for(int j = 0; j < column; j++){
				newBoard[i][j] = this.board[i][j];
			}
		}
		
		return newBoard;
		
	}
	
	/**
	 * Getter for getting board
	 * @return int[][]board
	 */
	public int[][] getBoard() {
		return board;
	}
	
	/**
	 * Copies a board from an input board
	 * @param board
	 */
	public void copyBoard(int[][] board) {
		for (int i = 0; i < row; i++) {
			for(int j = 0; j < column; j++){
				this.board[i][j] = board[i][j];
			}
		}
	}

	/**
	 * Sets board from input board
	 * @param board
	 */
	public void setBoard(int[][] board) {
		this.board = board;
		this.originBoard = this.getOriginalBoard();
	}
	
	/**
	 * Getter for getting board number
	 * @param row
	 * @param col
	 * @return board number
	 */
	public int getBoardNum(int row, int col) {
		return this.board[row][col];
	}
	
	/**
	 * Setter for setting board number
	 * @param row
	 * @param col
	 * @param Num
	 */
	public void setBoardNum(int row, int col, int Num) {
		this.board[row][col] = Num;
	}
	
}

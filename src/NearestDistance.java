import java.util.ArrayList;
import java.util.Iterator;

/**
 * Calculates heuristic value.
 * This is done by finding minimum distance between boxes and goals.
 * Then these values are summed up.
 * And also adds distance between closest box to the character.
 * Number of moves are not counted into calculation as it takes more time to
 * calculate the optimum path hence the path found by this heuristic value
 * will not be optimum
 * 
 * @author 
 *
 */
public class NearestDistance implements AIStrategy {
	@Override
	public int getH(Position character, ArrayList<Position> featurePos, int numMove) {
		int value = 0;

		// Split features to box and goal
		ArrayList<Position> boxPos = new ArrayList<Position>();
		ArrayList<Position> goalPos = new ArrayList<Position>();
		
		Iterator<Position> itr = featurePos.iterator();
		while (itr.hasNext()) {
			Position pos = itr.next();
			if (pos.getType().equals("box")) {
				//System.out.println(pos.getType());

				boxPos.add(pos);
			} else if (pos.getType().equals("goal")) {
				//System.out.println(pos.getType());

				goalPos.add(pos);
			}
		}
		// add up manhattan distance between box and nearest goal;
		Iterator<Position> boxItr = boxPos.iterator();
		while (boxItr.hasNext()) {
			Position pos = boxItr.next();
			value += this.distanceNearestFeat(pos, goalPos);
			//System.out.println(Integer.toString(value));
		}
		// add player's distance to nearest box
		value += this.distanceNearestFeat(character, boxPos);
		//System.out.println(Integer.toString(value));
		
		/* Uncommenting this will find optimum path
		value = numMove * value;
		*/
		return value;
	}
	
	/**
	 * Calculates manhattan distance between two nearest feature
	 * @param feature
	 * @param posArray
	 * @return distance between two nearest feature
	 */
	// Calculate manhattan distance between two nearest features
	public int distanceNearestFeat(Position feature, ArrayList<Position> posArray) {
		int dis = 0;
		int counter = 0;
		int row = feature.getRow();
		int column = feature.getColumn();
		Iterator<Position> itr = posArray.iterator();
		while (itr.hasNext()) {
			Position pos = itr.next();
			int calDist = 0;
			int posRow = pos.getRow();
			int posCol = pos.getColumn();
			if (posRow >= row) {
				calDist += (posRow - row);
			} else {
				calDist += (row - posRow);
			}
			
			if (posCol >= column) {
				calDist += (posCol - column);
			} else {
				calDist += (column - posCol);
			}
			//System.out.println("Caldist: " + Integer.toString(calDist));
			//System.out.println("dis " + Integer.toString(dis));
			if (counter == 0) {
				dis = calDist;
			} else if (dis >= calDist) {
				dis = calDist;
			}
			//System.out.println("new dis " + Integer.toString(dis));
			counter++;

		}
		
		//System.out.println(Integer.toString(dis));

		return dis;
	}

}

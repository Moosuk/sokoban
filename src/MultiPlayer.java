
public class MultiPlayer {
	int player2 = 7;
	/**
	 * This function doubles and copy the original board to the multi player board
	 * @param board original board 
	 * @return the board we need for the multi player.
	 */
	public GameBoard MutiPlayerBoard (GameBoard board) {
		GameBoard newBoard = new GameBoard(board.getRow(), board.getColumn()*2);
		//newBoard.setColumn(board.getColumn()*2);		
		for(int i = 0; i < board.getRow(); i++) {
			for(int j = 0; j < board.getColumn();j++) {
				newBoard.setBoardNum(i, j, board.getBoardNum(i, j));
				if(board.getBoardNum(i, j) == 1){
					newBoard.setStartingLocation(i, j);
					newBoard.setBoardNum(i, j+board.getColumn(), 7);
					newBoard.setPlayer2StartingLocation(i, j+board.getColumn());
				}
				else if(board.getBoardNum(i, j) == 5){
					newBoard.setBoardNum(i, j+board.getColumn(), 9);
				}
				else{
					newBoard.setBoardNum(i, j+board.getColumn(), board.getBoardNum(i, j));	
				}
				
			}
		}
		newBoard.setBoard(newBoard.getBoard());
		newBoard.setMultiOn();
		return newBoard;
	}
}
